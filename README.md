# Combine Markdown Chapters To One Markdown Book - combined-markdown.ps1

Run this .ps1 file from within Windows Powershell using: `.\combined-markdown.ps1`

## What it does:

# Use this from Windows PowerShell in a folder with markdown chapters named chapter1.md, chapter2.md, etc.
# It will combine all the files in that folder to `combined.md`


---

# Split Markdown To Chapters - `split-chapters.lua`

## What this script does:

`split-chapters.lua` is a script that can split a markdown book into seperate chapters when run in conjunction with `pandoc` on windows

---
## Pre-requisites:

- This assumes you have a markdown formatted book and `pandoc` is installed on Windows.
- Every chapter must have a title present in the format `# chapter title #`.
- There must be an empty line before the title or it will ignore it.
- Nothing else should have that single hashtag format.
- double hashtags ## will be ignored.

---
## Steps:

1. Save `split-chapters.lua` to the same folder as your `your-book.md`
2. Open a windows command prompt and cd to the folder containing your markdown book.
3. `mkdir` a folder within that folder called "chapters" (this will hold the individual chapters numbered sequentially as "chapter1.md" etc.. it will not use the chapter title)
4. In the command prompt window run `pandoc --lua-filter=split-chapters.lua -f markdown -t markdown "your-book.md" -s`

---
## Things to note:

NOTE: to fix issues with exported `chapters.md` having `CRLF` after each line, run this: `pandoc -f markdown -t markdown --wrap=none input.md -o output.md`

The chapter numbers created wont necessarily match your chapter numbers, but will be in the order the ``# chapter titles #`` are found within the original file.

For example, this is my `chapter2.md` file because `chapter1.md` was the book front page and introduction paragraph:

The original text in my my-book.md looked like this (included a blank line before the title):
```Text

# Introduction #
In March 2007, aged 40, and in the midst of...
```

but that became `chapter2.md` and looked like this:

```Text
# Chapter 1

# Introduction

In March 2007, aged 40, and in the midst of...
```

Just something to be aware of as the numbering might get confusing.

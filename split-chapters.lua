-- Define a function to split the markdown document into chapters
function SplitChapters(doc)
  -- Create a table to hold the chapters
  local chapters = {}
  
  -- Initialize a new chapter
  local current_chapter = {
    content = {},
    meta = {}
  }
  
  -- Initialize a chapter counter
  local chapter_counter = 1
  
  -- Iterate over the document's blocks
  for _, block in ipairs(doc.blocks) do
    -- Check if the block is a header with level 1 (single #)
    if block.t == 'Header' and block.level == 1 then
      -- If a new chapter is encountered, add the previous chapter to the chapters table
      if #current_chapter.content > 0 then
        table.insert(chapters, current_chapter)
      end
      
      -- Create a new chapter with a sequential chapter number
      local chapter_title = "Chapter " .. chapter_counter
      current_chapter = {
        content = {pandoc.Header(1, pandoc.Str(chapter_title)), pandoc.Str("\n\n")},
        meta = block.meta
      }
      chapter_counter = chapter_counter + 1
    end
    
    -- Append the block to the current chapter
    table.insert(current_chapter.content, block)
  end
  
  -- Add the last chapter to the chapters table
  if #current_chapter.content > 0 then
    table.insert(chapters, current_chapter)
  end

  -- Write each chapter to its own file
  for i, chapter in ipairs(chapters) do
    local chapter_filename = string.format("chapters/chapter%d.md", i)
    local chapter_file = io.open(chapter_filename, "w")
    local chapter_doc = pandoc.Pandoc(chapter.content, doc.meta)
    chapter_file:write(pandoc.write(chapter_doc, "markdown"))
    chapter_file:close()
  end

  -- Return an empty Pandoc document (since output is written to files)
  return pandoc.Pandoc({}, doc.meta)
end

-- Register the SplitChapters function as a Lua filter
return {
  {
    Pandoc = SplitChapters
  }
}

# Use this from Windows PowerShell in a folder with markdown chapters named chapter1.md, chapter2.md, etc.
# It will combine all the files in that folder to `combined.md`

# Get the current script directory
$scriptDir = Split-Path -Parent $MyInvocation.MyCommand.Path

# Get all Markdown files in the current directory that match the pattern chapterX.md
$files = Get-ChildItem -Path $scriptDir -Filter "chapter*.md"

# Sort files numerically based on their chapter number
$sortedFiles = $files | Sort-Object {
    if ($_.BaseName -match 'chapter(\d+)') {
        return [int]($matches[1])
    } else {
        return [int]::MaxValue
    }
}

# Combine the content of all files
$combinedContent = $sortedFiles | ForEach-Object {
    Get-Content $_.FullName
    "`n`n"  # Add two newlines between files
}

# Write the combined content to a new file
$combinedContent | Set-Content -Path "$scriptDir\combined.md"

# Output a success message
Write-Output "Combined Markdown files into combined.md"




